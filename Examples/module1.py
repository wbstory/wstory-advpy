import math
import module2
#from module2 import circumference, answer

question = "Hello"
answer = 42

def areaofCircle(radius):
	return math.pi*radius**2

if __name__ == "__main__":
	print(question)
	print(answer)
	area = areaofCircle(3)
	print("area = {:.2f}".format(area))
	print(module2.question)
	print(module2.answer)
	print(circumference(3))
