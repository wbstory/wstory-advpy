import random
import sys
import os

#Opening the file with the 20 words and assigning it to the list words
with open("words.txt", "r") as f:
	words = f.readlines()
playAgain = True

#setting important information such as 'invalid character' when the user
#inputs values they shouldn't
msg = ""

#initialize all the hangman strings sequences into list initHangman
initHangman = [ "    _______\n" + \
		"    |     |\n" + \
		"    |\n" + \
		"    |\n" + \
		"    |\n" + \
		"    |\n" + \
		"____|____  \n"]

initHangman.append( "    _______\n" + \
		"    |     |\n" + \
		"    |   (o o)\n" + \
		"    |\n" + \
		"    |\n" + \
		"    |\n" + \
		"____|____  \n")

initHangman.append( "    _______\n" + \
		"    |     |\n" + \
		"    |   (o o) \n" + \
		"    |     |\n" + \
		"    |\n" + \
		"    |\n" + \
		"____|____  \n")
	
initHangman.append( "    _______\n" + \
		"    |     |\n" + \
		"    |   (o o)\n" + \
		"    |    \\|/\n" + \
		"    |\n" + \
		"    |\n" + \
		"____|____  \n")
	
initHangman.append( "    _______\n" + \
		"    |     |\n" + \
		"    |   (o o)\n" + \
		"    |    \\|/\n" + \
		"    |     |\n" + \
		"    |\n" + \
		"____|____  \n")

initHangman.append( "    _______\n" + \
		"    |     |\n" + \
		"    |   (o o)\n" + \
		"    |    \\|/\n" + \
		"    |     |\n" + \
		"    |    / \\\n" + \
		"____|____  \n")
	
initHangman.append("    _______\n" + \
		"    |     |\n" + \
		"    |   (x x)\n" + \
		"    |    \\|/\n" + \
		"    |     |\n" + \
		"    |    / \\\n" + \
		"____|____  \n")

#This is the game loop, the game will restart if the user chooses
while (playAgain == True):
	#Creates a random number between 0 and 19 (20 total options)
	ran = random.randint(0, 19)
	msg= ""

	#hangStage is the stage the hangman is in, dead is when the user
	#is out of chances, gLetters is the list of already guessed letters
	hangStage = 0
	dead = False
	gLetters = [" "]
	
	#cWord is the randomly chosen word from the list of 20
	#check answer counts the # of letters in the chosen word so that
	#the game will end when all letters are guessed
	cWord = list(words[ran].rstrip())
	checkAns = len(cWord)
	
	#gWord is the number of letters in the chosen word viewed with '_'s
	#the letters fill in as the user guesses them
	gWord = ["_"]
	for i in range(len(cWord)-1):
		gWord.append("_")

	#If the user isn't dead and they want to play again
	while (dead==False and playAgain == True):
		dead = True
		os.system('clear')
		print(initHangman[hangStage])
		for x in range(len(gWord)): 
			print gWord[x],
		
		print("\n")
		print(msg)	
		msg = ""
		
		#prints the guesses letters
		print("Used Guesses:"),
		for x in range(len(gLetters)):
			if (gLetters[x] != ""):
				print(gLetters[x]+" "),
		print("")

		#Allows the user to guess a letter
		gLetter = str(raw_input("Guess a letter: "))
		
		#Checks the users input to see if it is valid
		if((len(gLetter) > 1) or not (gLetter.lower() <= "z" and gLetter.lower() >= "a")):
			dead = False		
			msg = "**Please enter one appropriate letter**"
		
		#Checks to see if the letter is in the chosen word
		if(dead != False):
			for j in range(len(cWord)):
				if (cWord[j].lower() == gLetter.lower()):
					gWord[j] = cWord[j] 
					dead = False
					checkAns -= 1
				if gLetter.lower() in gLetters:
					msg = "You already guessed that Letter!"
					dead = False
					gLetter = ""
					break
		
			gLetters.append(gLetter)
			
			#If the letter the user guessed is wrong then the
			#hangstage is incremented and dead is set to false
			if (dead == True):
				hangStage +=1
				dead = False
			if (hangStage == len(initHangman)-1):
				msg = "You Lose!!"
				dead = True
			if (checkAns <= 0):
				msg = "You Win!!"
				dead = True
		
			#If the game is over, ask the user if they want to
			#play again. If play again, reset all variables
			if (dead):
				os.system('clear')
				print(initHangman[hangStage])
				for x in range(len(cWord)): 
					print cWord[x], 
				print("\n")
				print(msg)
				if (raw_input("Do you want to play again ('y' or 'n'): ")=="y"):
					dead = False
					hangStage = 0
					break
				else:
					playAgain = False
