Overview:
This program is a simple hangman program that randomly chooses out of a list of 20 words. The premise of the game, like other hangman games, is to guess the word one letter at a time. If you guess the wrong letter, your man becomes more complete. If your man completes, he dies.

Running hangman:
 - From your console (after downloading hangman.py and words.txt), run the following command while in the correct directory: $ python hangman.py

How to Play:
 - The basic hangman layout will be printed on the screen when you start.
 - The console will prompt you to enter a letter, you can only enter 1 letter (no numbers or special characters)
 - Enter a guess and press enter until the game is over (man is hung or survives)
 - After that you are given the option to play again, enter a 'n' if you don't want to play or 'y' if you do.
