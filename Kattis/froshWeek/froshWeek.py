import sys
import time

def test():
    pass

def sort(alist):
    if len(alist)>1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        sort(lefthalf)
        sort(righthalf)

        i=0
        j=0
        k=0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] > righthalf[j]:
                alist[k]=lefthalf[i]
                i=i+1
            else:
                alist[k]=righthalf[j]
                j=j+1
            k=k+1

        while j < len(righthalf):
            alist[k]=righthalf[j]
            j=j+1
            k=k+1

        while i < len(lefthalf):
            alist[k]=lefthalf[i]
            i=i+1
            k=k+1

        
    return alist

def manageTime(tasks, intervals):
    count = 0
    count2 = 0
    for i in range(200000):
        if (i > len(tasks)-1 or count2 > len(intervals)-1):
            break
        if (tasks[i] <= intervals[count2]):
            count2 += 1
            count += 1
        if (len(tasks) == 0 or len(intervals) == 0):
            break
    return count

if __name__ == "__main__":
    #start = time.time()
    if(len(sys.argv) == 2 and sys.argv[1] == "test"):
        test()
    else:
        nm = input()
        
        tasks = input().split()
        intervals = input().split()
        
        tasks = sort(tasks)
        intervals = sort(intervals)

        print(manageTime(tasks, intervals))
    #end = time.time()
    #print("program: ",end-start)