import sys

def flip(snappers, snapperNo, snapCheck):
    if (snapCheck == snapperNo-1):
        if(snappers[snapCheck]):
            return 0
        else:
            return 1
    else:
        if (snappers[snapCheck]):
            snappers[snapCheck+1] = flip(snappers, snapperNo, snapCheck+1)
            return 0
        else:
            return 1

def isLightOn(t1):
    snapperNo = t1[0]
    flips = t1[1]
    snappers = []

    for i in range(snapperNo):
        snappers.append(0)
    for i in range(flips):
        if (snappers[0]):
            if (snapperNo != 1):
                snappers[1] = flip(snappers, snapperNo, 1)
            snappers[0] = 0
        else:
            snappers[0] = 1

    for i in range(snapperNo):
        if (snappers[i] == 0):
            return "OFF"
        pass
    return "ON"


def test():
    assert isLightOn([4, 55]) == "OFF", "Test 1 didn't pass..."
    assert isLightOn([4, 63]) == "ON", "Test 2 didn't pass..."
    assert isLightOn([5, 3]) == "OFF", "Test 3 didn't pass..."
    assert isLightOn([2, 7]) == "ON", "Test 4 didn't pass..."
    print("All Test Cases Pass!!")

if __name__ == "__main__":
    if(len(sys.argv) == 2 and sys.argv[1] == "test"):
        test()
    else:
        testNo = 1
        tests = input()
        testInfo = []
        for i in range(int(tests)):
            info = input()
            info = info.split()
            testInfo.append([int(info[0]), int(info[1])])
        for i in range(len(testInfo)):
            print("Case #", testNo, ": ", isLightOn(testInfo[i]), sep='')
            testNo = testNo + 1