import sys

def sort(names):
    n1 = []
    n2 = []
    n3 = []

    half = int(len(names)/2)
    if (len(names) > 1):
        n1.extend(sort(names[0:half]))
        n2.extend(sort(names[half:len(names)]))
    elif (len(names) == 1):
        return names

    while(len(n1) != 0 or len(n2) != 0):
        if(len(n1) == 0 and len(n2) > 0):
            n3.append(n2[0])
            n2.remove(n2[0])
        elif(len(n2) == 0 and len(n1) > 0):
            n3.append(n1[0])
            n1.remove(n1[0])
        elif n1[0][0:2] < n2[0][0:2]:
            n3.append(n1[0])
            n1.remove(n1[0])
        elif n1[0][0:2] > n2[0][0:2]:
            n3.append(n2[0])
            n2.remove(n2[0])
        elif(len(n1) != 0):
            n3.append(n1[0])
            n1.remove(n1[0])
        else:
            n3.append(n2[0])
            n2.remove(n2[0])
    return n3

def test():
    words = ["Jill", "Jimmy", "John", "Albert", "Beau", "Fred"]
    words1 = ["Jessie", "Ben", "Berach"]
    words2 = ["Pill", "Fran", "Jameson", "Zulow", "Hide"]
    words3 = ["jane", "jack", "jape", "jaan"]

    cWords = ["Albert", "Beau", "Fred", "Jill", "Jimmy", "John"]
    cWords1 = ["Ben", "Berach", "Jessie"]
    cWords2 = ["Fran", "Hide", "Jameson", "Pill", "Zulow"]
    cWords3 = ["jane", "jack", "jape", "jaan"]

    assert (sort(words) == cWords), "Sorting algorithm (test1) didn't execute correctly."
    assert (sort(words1) == cWords1), "Sorting algorithm (test2) didn't execute correctly."
    assert (sort(words2) == cWords2), "Sorting algorithm (test3) didn't execute correctly."
    assert (sort(words3) == cWords3), "Sorting algorithm (test4) didn't execute correctly."

    
    print("All Test Cases Pass...")

if __name__ == "__main__":
    if(len(sys.argv) == 2 and sys.argv[1] == "test"):
        test()
    else:
        num = input()
        namesPacked = []
        while (int(num) != 0):
            names = []
            for i in range(int(num)):
                names.append(input())
            namesPacked.append(names)
            num = input()
        for i in range(len(namesPacked)):
            print(repr(sort(namesPacked[i])).replace(',','\n').replace('[','').replace(']','').replace('\'','').replace(' ','').rstrip())
            if (i != len(namesPacked)-1):
                print("")