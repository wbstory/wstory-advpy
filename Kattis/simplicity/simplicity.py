import sys

def test():
    assert myParse("tryout")==3, "Test Case 1 Failed..."
    assert myParse("aaaabbbccd")==3, "Test Case 2 Failed..."
    assert myParse("aaabbcdddd")==3, "Test Case 3 Failed..."
    assert myParse("abbbbcccdd")==3, "Test Case 4 Failed..."
    assert myParse("a")==0, "Test Case 5 Failed..."
    assert myParse("ab")==0, "Test Case 6 Failed..."
    assert myParse("aaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbb")==0, "Test Case 7 Failed..."
    assert myParse("asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf")==50, "Test Case 7 Failed..."
    print("All Test Cases Passed...")

def myCount(word):
    temp = []
    for i in range(len(word)):
        temp.append(word[i])
    
    myCount = 0
    while (len(temp) != 0):
        letter = temp[0]
        i = 0
        while i < len(temp):
            if (temp[i]==letter):
                temp.remove(letter)
            else:
                i += 1
        myCount += 1
    return myCount
def myParse(word):
    count = 0
    min = word.count(word[0])
    minLetter = word[0]
    while (myCount(list(word)) > 2):
        for i in range(len(word)):
            if (word.count(word[i]) < min):
                min = word.count(word[i])
                minLetter = word[i]
        word = list(word)
        j = 0
        while j < len(word):
            if (word[j] == minLetter):
                word.remove(minLetter)
                count += 1
            else:
                j += 1
        word = ''.join(word)

        if (len(word) != 0):
            min = word.count(word[0])
            minLetter = word[0]
    return count


if __name__ == "__main__":
    if(len(sys.argv) == 2 and sys.argv[1] == "test"):
        test()
    else:
        word = input()
        print(myParse(word))
