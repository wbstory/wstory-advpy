import sys

def printM(matrix):
    for i in range(4):
        for j in range(4):
            print(matrix[i][j], " ", end='')
        print("")

def uMove(matrix, bMove):
    if (bMove == 0):
        for i in range(4):
            j = 0
            while (j < 3):
                if (matrix[i][j] == 0 and matrix[i][j+1] != 0):
                    matrix[i][j] = matrix[i][j+1]
                    matrix[i][j+1] = 0
                    j = -1
                j += 1
        for i in range(4):
            for j in range(3):
                if (matrix[i][j] == matrix[i][j+1]):
                    matrix[i][j] = matrix[i][j] + matrix[i][j+1]
                    for k in range(j,2):
                        matrix[i][k+1] = matrix[i][k+2]
                    matrix[i][3] = 0
    if (bMove == 1):
        for i in range(4):
            j = 0
            while (j < 3):
                    if (matrix[j][i] == 0 and matrix[j+1][i] != 0):
                        matrix[j][i] = matrix[j+1][i]
                        matrix[j+1][i] = 0
                        j = 0
                    j += 1
        for i in range(4):
            for j in range(3):
                if(matrix[j][i] == matrix[j+1][i]):
                    matrix[j][i] = matrix[j+1][i] + matrix[j][i]
                    for k in range(j,2):
                        matrix[k+1][i] = matrix[k+2][i]
                    matrix[3][i] = 0
    if (bMove == 2):
        for i in range(4):
            j = 3
            while (j > 0):
                if (matrix[i][j] == 0 and matrix[i][j-1] != 0):
                    matrix[i][j] = matrix [i][j-1]
                    matrix[i][j-1] = 0
                    j = 3
                j -= 1
        for i in range(4):
            for j in range(3, -1, -1):
                if (j != 0):
                    if (matrix[i][j] == matrix[i][j-1]):
                        matrix[i][j] *= 2
                        matrix[i][j-1] = 0
                        for k in range(j, 1, -1):
                            matrix[i][k-1] = matrix[i][k-2]
                        matrix[i][0] = 0
    if (bMove == 3):
        for i in range(4):
            j = 3
            while (j > 0):
                if (matrix[j][i] == 0 and matrix[j-1][i] != 0):
                    matrix[j][i] = matrix[j-1][i]
                    matrix[j-1][i] = 0
                    j = 3
                j -= 1
        for i in range(4):
            for j in range(3, -1, -1):
                if (matrix[j][i] == matrix[j-1][i]):
                    matrix[j][i] *= 2
                    matrix[j-1][i] = 0
                    for k in range (j, 1, -1):
                        matrix[k-1][i] = matrix[k-2][i]
                    matrix[0][i] = 0
    return matrix

def test():
    matrix = [[2, 2, 2, 2], [64, 0, 0, 64], [0, 32, 32, 0], [0, 128, 0, 128]]
    matrix1 = [[2, 2, 2, 2], [64, 0, 0, 64], [0, 32, 32, 0], [0, 128, 0, 128]]
    matrix2 = [[2, 2, 2, 2], [64, 0, 0, 64], [0, 32, 32, 0], [0, 128, 0, 128]]
    matrix3 = [[2, 2, 2, 2], [64, 0, 0, 64], [0, 32, 32, 0], [0, 128, 0, 128]]

    secMatrix = [[0, 2, 2, 4], [2048, 2048, 512,512], [1024, 512, 0, 2], [0, 0, 2, 0]]
    secMatrix1 = [[0, 2, 2, 4], [2048, 2048, 512,512], [1024, 512, 0, 2], [0, 0, 2, 0]]
    secMatrix2 = [[0, 2, 2, 4], [2048, 2048, 512,512], [1024, 512, 0, 2], [0, 0, 2, 0]]
    secMatrix3 = [[0, 2, 2, 4], [2048, 2048, 512,512], [1024, 512, 0, 2], [0, 0, 2, 0]]
    
    cMatrix0 = [[4, 4, 0, 0], [128, 0, 0, 0], [64, 0, 0, 0], [256, 0, 0, 0]]
    cMatrix1 = [[2, 2, 2, 2], [64, 32, 32, 64], [0, 128, 0, 128], [0, 0, 0, 0]]
    cMatrix2 = [[0, 0, 4, 4], [0, 0, 0, 128], [0, 0, 0, 64], [0, 0, 0, 256]]
    cMatrix3 = [[0, 0, 0, 0], [2, 2, 0, 2], [64, 32, 2, 64], [0, 128, 32, 128]]
    
    secMatrix0 = [[4, 4, 0, 0], [4096, 1024, 0, 0], [1024, 512, 2, 0], [2, 0, 0, 0]]
    secMatrix1 = [[2048, 2, 2, 4], [1024, 2048, 512, 512], [0, 512, 2, 2], [0, 0, 0, 0]]
    secMatrix2 = [[0, 0, 4, 4], [0, 0, 4096, 1024], [0, 1024, 512, 2], [0, 0, 0, 2]]
    secMatrix3 = [[0, 0, 0, 0], [0, 2, 2, 4], [2048, 2048, 512, 512], [1024, 512, 2, 2]]

    assert uMove(matrix, 0) == cMatrix0, "Test 1.0 did not Pass..."
    assert uMove(matrix1, 1) == cMatrix1, "Test 1.1 did not Pass..."
    assert uMove(matrix2, 2) == cMatrix2, "Test 1.2 did not Pass..."
    assert uMove(matrix3, 3) == cMatrix3, "Test 1.3 did not Pass..."

    assert uMove(secMatrix, 0) == secMatrix0, "Test 2.0 did not Pass..."
    assert uMove(secMatrix1, 1) == secMatrix1, "Test 2.1 did not Pass..."
    assert uMove(secMatrix2, 2) == secMatrix2, "Test 2.2 did not Pass..."
    assert uMove(secMatrix3, 3) == secMatrix3, "Test 2.3 did not Pass..."

    print("All Test Cases Passed!")
if __name__ == "__main__":
    if(len(sys.argv) == 2 and sys.argv[1] == "test"):
        test()
    else:
        matrix = []
        for i in range(4):
            matrix.append(input().split())

        for i in range(4):
            for j in range(4):
                matrix[i][j] = int(matrix[i][j])
        printM(uMove(matrix, int(input())))