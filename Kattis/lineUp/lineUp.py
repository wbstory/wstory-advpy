import sys

def test():
    pass

def order(names):
    checkNum = -1
    for i in range((len(names)-1)):
        if (names[i] < names[i+1] and (checkNum == -1 or checkNum == 0)):
            checkNum = 0
        elif (names[i] > names[i+1] and (checkNum == -1 or checkNum == 1)):
            checkNum = 1
        else:
            return "NEITHER"
    if (checkNum == 0):
        return "INCREASING"
    else:
        return "DECREASING"

if __name__ == "__main__":
    if(len(sys.argv) == 2 and sys.argv[1] == "test"):
        test()
    else:
        num = int(input())
        names = []

        for i in range(num):
            names.append(input())
        
        print(order(names))