The problems solved were all kattis problems.

Included in each kattis problem folder are screenshots of the description of the problem, number of tries it took to solve the problem, and the solution info. 

The kattis problems completed were:
2048 		(difficulty: 2.2)	Test Cases Implemented
fallingapart 	(difficulty: 1.8)
froshweek2 	(difficulty: 2.6)
lineup		(difficulty: 1.8)
snappereasy 	(difficulty: 2.6)	Test Cases Implemented
sortofsorting	(difficulty: 2.5)	Test Cases Implemented
simplicity	(difficulty: 2.7)	Test Cases Implemented
