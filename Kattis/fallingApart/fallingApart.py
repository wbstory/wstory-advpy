import sys

def getMax(nums):
    maxI = 0
    for i in range(len(nums)):
        if (nums[i] > maxI):
            maxI = nums[i]
    return maxI

def seperate(nums):
    alice = 0    
    bob = 0

    while (len(nums) != 0):
        max = getMax(nums)
        alice += max
        nums.remove(max)
        if (len(nums)!=0):
            max = getMax(nums) 
            bob += max
            nums.remove(max)
    print(alice, bob)

def test():
    pass

if __name__ == "__main__":
    if(len(sys.argv) == 2 and sys.argv[1] == "test"):
        test()
    else:
        num = int(input())
        nums = input().split()

        for i in range(num):
            nums[i] = int(nums[i])
        
        seperate(nums)